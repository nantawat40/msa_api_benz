package com.gable.msa.api;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;

import lombok.extern.java.Log;

@SpringBootTest
@Log
class MsaApiApplicationTests {

	@Value(value="${server.port}")	
	private int port;
		
	
	@Value(value = "${spring.data.rest.base-path}")   // nice trick to get basePath from application.properties
	private String basePath;
	
//	@Autowired
//	private TestRestTemplate restTemplate;
	
	@Value(value = "${server.mapping.path}")   // nice trick to get basePath from application.properties
	private String mapPath;	
	
	@Test
	void CurrentServerPort() throws Exception {
		log.severe("Show URL http://"+basePath+":" + port);
		assertNotNull(port);
	}		
	
	@Test
	void CurrentServerPortPath() throws Exception {
		log.severe("Show URL http://"+basePath+":" + port +"/"+mapPath);
		assertNotNull(mapPath);
	}		
		
	@ParameterizedTest
	@ValueSource(strings = {"users/status/check","msa"}) // six numbers	
	void URL_Mapping(String url) throws Exception  {
		log.severe("http://localhost:" + port);
		final String baseUrl = "http://localhost:"+port+"/"+url+"/";
		
		TestRestTemplate restTemplate = new TestRestTemplate();
		
		if (url.contains("users"))
			assertThat(restTemplate.getForObject(baseUrl, String.class)).contains("Working for users asdfs");
		else
			assertThat(restTemplate.getForObject(baseUrl, String.class)).contains("\"Hello, World!\"");		
	}	

}
