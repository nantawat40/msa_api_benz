package com.gable.msa.restservice;


import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;


@Data
@AllArgsConstructor
public class Greeting {
	private long id;
	private String content;
}
