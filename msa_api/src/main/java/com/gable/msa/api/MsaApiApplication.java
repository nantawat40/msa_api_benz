package com.gable.msa.api;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.context.annotation.ComponentScan;

//@EnableEurekaServer
@SpringBootApplication
@ComponentScan({"com.gable.msa.api.controller"})
public class MsaApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsaApiApplication.class, args);
		
	}
}
