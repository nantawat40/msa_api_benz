package com.gable.msa.api.controller;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MSAControllerRepository extends JpaRepository<Long, String> {

}
