package com.gable.msa.api.controller;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gable.msa.restservice.Greeting;

import lombok.extern.java.Log;

@Log
@RestController
public class MSAController<T> {

	private final MSAControllerRepository repository;
	
	private static final String MSG = "Hello, %s!";
	private final AtomicLong counter = new AtomicLong();
	
	MSAController(MSAControllerRepository repository) {
	    this.repository = repository;
	  }
	

	@GetMapping("/Greeting")
	public List<T> all() {
		return (List)repository.findAll();
	}
	
	@PostMapping("/Greeting")
	Greeting newEmployee(@RequestBody Greeting newGreeting) {
		return repository.save(newGreeting);
	}
	  
	@GetMapping("/users/status/check")
	public String usersStatusCheck() {
		return "Working for users asdfs";
	}
	
	@GetMapping("/${server.mapping.path}")
	public Greeting msa(@RequestParam(value = "name", defaultValue = "World") String name) {
		log.severe("Something's wrong here");
		return new Greeting(counter.incrementAndGet(), String.format(MSG, name));
	}	
}
