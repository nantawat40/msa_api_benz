package com.gable.msa.api.controller;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.gable.msa.restservice.Greeting;

import lombok.extern.java.Log;

@Log
@Configuration
class LoadDatabase {


  @Bean
  CommandLineRunner initDatabase(MSAControllerRepository repository) {

    return args -> {
      log.info("Preloading " + repository.save(new Greeting(1001, "burglar")));
      log.info("Preloading " + repository.save(new Greeting(1002, "thief")));
    };
  }
}