package com.gable.msa.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

@ControllerAdvice
public class MSAControllerNotFoundAdvice {
	@ResponseBody
	@ExceptionHandler(MSAControllerNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	String employeeNotFoundHandler(MSAControllerNotFoundException ex) {
		return ex.getMessage();
	}
	
}
