package com.gable.msa.controller;


import javax.validation.Valid;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.gable.msa.model.RequestJSON;
import com.gable.msa.model.ResponseJSON;

import lombok.extern.java.Log;

@Log
@RestController
public class MSAController {

	@GetMapping("/${server.mapping.path}")
	public String CheckStatus() {
		RequestJSON request = new RequestJSON();
		return request.getVersion();
	}
	
	@GetMapping("/Request_Test")
	public RequestJSON all() {
		return new RequestJSON();
	}
	
	@GetMapping("/Request_TestC")
	public RequestJSON call() {
		return new RequestJSON();
	}	
	
	@PostMapping("/${server.mapping.path}")
	public ResponseJSON newRequest(@Valid @RequestBody RequestJSON RequestJSON) {
		
		//Check ticket_id already request		
		
		//Mapping value from request to response
		ResponseJSON response = new ResponseJSON();
		response.setRequst_time(RequestJSON.getRequst_time());
		response.setTicket_id(RequestJSON.getTicket_id());
		response.setUsername(RequestJSON.getUsername());
		response.setId(RequestJSON.getId());
		response.setStatus(1);
		response.setDesc("in-progress");
		
		log.info("POST : " + RequestJSON.toString());
		log.info("POST : " + response.toString());
		
		return response;
	}
}
