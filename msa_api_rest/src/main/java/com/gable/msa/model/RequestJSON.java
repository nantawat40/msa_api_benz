package com.gable.msa.model;


import java.util.Date;

import javax.validation.constraints.NotBlank;

import org.springframework.data.annotation.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestJSON {
	private @Id Long id;
	private final String version = "0.1.0";
	private String ticket_id;
	
	@NotBlank(message = "username is mandatory")
	private String username;	
	
	@NotBlank(message = "endpoint is mandatory")
	private String endpoint;
//	private String token_no;	
	private final Date requst_time = new Date();
	
	
	public RequestJSON(String endpoint) {
		this.endpoint = endpoint; 		
	}
	
}
