package com.gable.msa.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import com.gable.msa.model.CustomerRepository;

@SpringBootApplication
@ComponentScan({"com.gable.msa.controller"})
@EnableSwagger2
@EnableJpaAuditing
public class MsaApiTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsaApiTestApplication.class, args);
	}
	
	@Bean
	public Docket swaggerDemoApi()
	{
		return new Docket(DocumentationType.SWAGGER_2).
						select().
						apis(RequestHandlerSelectors.
						basePackage("com.gable.msa.controller")).build();
	}
	
}
