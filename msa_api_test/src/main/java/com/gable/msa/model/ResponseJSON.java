package com.gable.msa.model;

import java.util.Date;

import org.springframework.data.annotation.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseJSON {
	private @Id Long id;
	private final String version = "0.1.0";
	private String ticket_id;
	private String username;
	private int status;
	private String desc;
	private Date requst_time;
	private Date response_time = new Date();
	
}
