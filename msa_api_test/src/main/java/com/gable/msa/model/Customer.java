package com.gable.msa.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "customer")
public class Customer implements Serializable {
	  private static final long serialVersionUID = -3009157732242241606L;
	  @Id
	  @GeneratedValue(strategy = GenerationType.AUTO)
	  private long id;
	  
	  @Column(name = "firstname",nullable = true)
	  private String firstName;
	  
	  @Column(name = "lastname",nullable = true)
	  private String lastName;
	 
	  public Customer(String firstName, String lastName)
	  {
		this.firstName = firstName;
		this.lastName = lastName;
	  }
}
