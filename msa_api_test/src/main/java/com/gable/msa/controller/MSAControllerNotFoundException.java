package com.gable.msa.controller;

public class MSAControllerNotFoundException extends RuntimeException {
	MSAControllerNotFoundException(Long id) {
		super("Could not find employee " + id);
	}
}
